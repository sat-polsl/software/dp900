# DP900 remote terminal

## Requirements:
- Python 3.8

## Usage
1. `. venv/bin/activate`
2. `pip install -r requirements.txt`
3. `python terminal.py`
