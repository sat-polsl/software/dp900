from enum import IntEnum
from pyvisa import Resource

class Channel(IntEnum):
    CH1 = 1
    CH2 = 2
    CH3 = 3

class ChannelState(IntEnum):
    ON = 1
    OFF = 0

class SCPI:
    """
    Class wrapping SCPI instructions

    Programming Guide: https://kr.rigol.com/Public/Uploads/uploadfile/files/20221014/20221014170202_6349258a6a31f.pdf
    """

    def __init__(self, resource: Resource) -> None:
        """
        Constructor.
        :param resource: pyvisa resource
        """
        self._resource = resource

    def set_remote(self) -> None:
        """
        Sets remote mode.
        :return:
        """
        self._resource.write('SYST:REM')

    def set_local(self) -> None:
        """
        Sets local mode.
        :return:
        """
        self._resource.write('SYST:LOC')

    def set_channel_current(self, channel: Channel, current: float) -> None:
        """
        Sets current limit for given channel.
        :param channel: Selected channel.
        :param current: Current limit in A.
        :return:
        """
        self._resource.write(f'SOUR{channel.value}:CURR {current:.2f}')

    def set_channel_voltage(self, channel: Channel, voltage: float) -> None:
        """
        Sets voltage limit for given channel.
        :param channel: Selected channel.
        :param voltage: Voltage limit in V.
        :return:
        """
        self._resource.write(f'SOUR{channel.value}:VOLT {voltage:.2f}')

    def get_channel_current(self, channel: Channel) -> float:
        """
        Gets current limit for given channel.
        :param channel: Selected channel.
        :return:
        """
        return float(self._resource.query(f'SOUR{channel.value}:CURR?'))

    def get_channel_voltage(self, channel: Channel) -> float:
        """
        Gets voltage limit for given channel.
        :param channel: Selected channel.
        :return:
        """
        return float(self._resource.query(f'SOUR{channel.value}:VOLT?'))

    def get_channel_voltage_measurement(self, channel: Channel) -> float:
        """
        Gets voltage measurement for given channel.
        :param channel: Selected channel.
        :return:
        """
        return float(self._resource.query(f'MEAS:VOLT? {channel.name}'))

    def get_channel_current_measurement(self, channel: Channel) -> float:
        """
        Gets current measurement for given channel.
        :param channel: Selected channel.
        :return:
        """
        return float(self._resource.query(f'MEAS:CURR? {channel.name}'))

    def get_channel_power_measurement(self, channel: Channel) -> float:
        """
        Gets power measurement for given channel.
        :param channel: Selected channel.
        :return:
        """
        return float(self._resource.query(f'MEAS:POWE? {channel.name}'))

    def enable_channel(self, channel: Channel) -> None:
        """
        Enables given channel.
        :param channel: Selected channel.
        :return:
        """
        self._resource.write(f'OUTP {channel.name},ON')

    def disable_channel(self, channel: Channel) -> None:
        """
        Disables given channel.
        :param channel: Selected channel.
        :return:
        """
        self._resource.write(f'OUTP {channel.name},OFF')

    def get_channel_state(self, channel: Channel) -> ChannelState:
        """
        Gets state of given channel.
        :param channel: Selected channel.
        :return:
        """
        return ChannelState(int(self._resource.query(f'OUTP? {channel.name}')))
