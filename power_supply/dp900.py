from power_supply.SCPI import *
from pyvisa import Resource

class dp900:
    """
    Class responsible for controlling Rigol DP900 power supply
    """

    def __init__(self, resource: Resource) -> None:
        """
        Constructor.
        :param resource: pyvisa resource
        """
        self._resource = resource
        self._scpi = SCPI(resource)
        self._scpi.set_remote()

    def __del__(self):
        self._scpi.set_local()

    def set_remote(self) -> None:
        """
        Sets remote mode.
        :return:
        """
        self._scpi.set_remote()

    def set_local(self) -> None:
        """
        Sets local mode.
        :return:
        """
        self._scpi.set_local()

    def set_channel_current(self, channel: Channel, current: float) -> None:
        """
        Sets current limit for given channel.
        :param channel: Selected channel.
        :param current: Current limit in A.
        :return:
        """
        self._scpi.set_channel_current(channel, current)

    def set_channel_voltage(self, channel: Channel, voltage: float) -> None:
        """
        Sets voltage limit for given channel.
        :param channel: Selected channel.
        :param voltage: Voltage limit in V.
        :return:
        """
        self._scpi.set_channel_voltage(channel, voltage)

    def get_channel_current(self, channel: Channel) -> float:
        """
        Gets current limit for given channel.
        :param channel: Selected channel.
        :return:
        """
        return self._scpi.get_channel_current(channel)

    def get_channel_voltage(self, channel: Channel) -> float:
        """
        Gets voltage limit for given channel.
        :param channel: Selected channel.
        :return:
        """
        return self._scpi.get_channel_voltage(channel)

    def get_channel_voltage_measurement(self, channel: Channel) -> float:
        """
        Gets voltage measurement for given channel.
        :param channel: Selected channel.
        :return:
        """
        return self._scpi.get_channel_voltage_measurement(channel)

    def get_channel_current_measurement(self, channel: Channel) -> float:
        """
        Gets current measurement for given channel.
        :param channel: Selected channel.
        :return:
        """
        return self._scpi.get_channel_current_measurement(channel)

    def get_channel_power_measurement(self, channel: Channel) -> float:
        """
        Gets power measurement for given channel.
        :param channel: Selected channel.
        :return:
        """
        return self._scpi.get_channel_power_measurement(channel)

    def enable_channel(self, channel: Channel) -> None:
        """
        Gets state of given channel.
        :param channel: Selected channel.
        :return:
        """
        return self._scpi.enable_channel(channel)

    def disable_channel(self, channel: Channel) -> None:
        """
        Sets state of given channel.
        :param channel: Selected channel.
        :param state: Selected state.
        :return:
        """
        self._scpi.disable_channel(channel)

    def get_channel_state(self, channel: Channel) -> ChannelState:
        """
        Gets state of given channel.
        :param channel: Selected channel.
        :return:
        """
        return self._scpi.get_channel_state(channel)
