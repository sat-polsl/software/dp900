from pyvisa import ResourceManager
from power_supply import dp900
from IPython.terminal.embed import InteractiveShellEmbed


def main():
    rm = ResourceManager()
    print('Enter resource IP address:')
    ip = input()
    resource = rm.open_resource(f'TCPIP::{ip}::INSTR')

    if resource is not None:
        ps = dp900(resource=resource)
        scope = {'ps': ps}
        shell = InteractiveShellEmbed(banner1='Power supply terminal', user_ns=scope)

        shell()
    else:
        print('Resource not found')


if __name__ == '__main__':
    main()
